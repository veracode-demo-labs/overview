# Overview

High-level guidelines and recommendations for the projects in this group

## Guidelines

- All projects should be self-contained.  No links to other projects.  Don't try to get fancy and have a single 'security settings' project apply to multiple source code projects.  Keep it simple so you can download a single project and everything works.

- At a minimum, projects should have
    - a readme file with instructions on how to build
    - a jenkinsfile for pipeline builds in Jenkins
    - Veracode static scan ('upload-only'/'fire-and-forget' is fine)
    - Veracode agent-based SCA scan
    - (optional) yaml-based build file (e.g., GitLab, CircleCI, etc.)
    - (very optional) deployment via Heroku or similar